package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoanApp {
    
    private FormatterService formatter;

    @Autowired
    public LoanApp(FormatterService formatter) {
        this.formatter = formatter;
    }

    public void displayLoan(String account, String type, int loan) {
        System.out.println("account: " + account);
        System.out.println("type: " + type);
        System.out.println("outstanding: " + formatter.format(loan));
    }

}