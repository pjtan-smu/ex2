package com.example.demo;

public class CurrencyFormatService implements FormatterService {
    
    @Override
    public String format(int number) {
        return "SGD " + number;
    }

}
