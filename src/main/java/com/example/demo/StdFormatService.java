package com.example.demo;


public class StdFormatService implements FormatterService {
    
    @Override
    public String format(int number) {
        return "$" + number;
    }

}
