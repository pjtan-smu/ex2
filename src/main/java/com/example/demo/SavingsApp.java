package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SavingsApp {
    
    private FormatterService formatter;

    @Autowired
    public SavingsApp(FormatterService formatter) {
        this.formatter = formatter;
    }

    public void displayBalance(String account, int balance) {
        System.out.println("account: " + account);
        System.out.println("balance: " + formatter.format(balance));
    }

}
