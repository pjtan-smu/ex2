package com.example.demo;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class EnhancedFormatterService implements FormatterService {

    @Override
    public String format(int number) {
        String str = String.format("%,d", (int)number);
        return "$"+str;
    }
    
}
