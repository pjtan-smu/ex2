package com.example.demo;

public interface FormatterService {
    public String format(int number) ;
}
